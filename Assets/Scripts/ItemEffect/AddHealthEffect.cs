using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CreatorKitCode;

public class AddHealthEffect : UsableItem.UsageEffect
{

    public int increment;
    

    public override bool Use(CharacterData user)
    {
        if (user.Stats.CurrentHealth == user.Stats.stats.health)
            return false;
        else if (user.Stats.CurrentHealth <= 4)
            return false;

        int num = Random.Range(1, 15);

        if (num <= 9)
        {
            user.Stats.ChangeHealth(increment);
               // LUCKY!
            VFXManager.PlayVFX(VFXType.Healing, user.transform.position);
        }
        else
        {
            user.Stats.ChangeHealth(-increment*2);
               // TOO BAD!
            VFXManager.PlayVFX(VFXType.Negative, user.transform.position);
        }

        return true;
    }
}
