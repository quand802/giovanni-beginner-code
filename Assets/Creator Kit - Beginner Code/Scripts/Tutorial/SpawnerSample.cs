﻿using UnityEngine;
using CreatorKitCode;


public class SpawnerSample : MonoBehaviour
{ 
    // Public Values
    public GameObject ObjectToSpawn;
    public int NumberOfPotions;

    // Scopes
    Vector3 spawnPosition;
    Vector3 direction;
    int RAD;
    int ANGLE;

    void Start()
    {
      LootAngle myLootAngle = new LootAngle(45);

        for (int i = 0; i < NumberOfPotions; i++)
        {
            spawnBottle(myLootAngle.NextAngle());
        }
           
    }

    void spawnBottle(int ang)
    {
       
        ANGLE = ang; // Get a random range for each loop
        RAD = Random.Range(1, 6); // Get a random radius for each loop

        direction = Quaternion.Euler(0, ANGLE, 0) * Vector3.right; 
        spawnPosition = transform.position + direction * RAD;
        Instantiate(ObjectToSpawn, spawnPosition, Quaternion.identity);
      
    }

}

public class LootAngle : SpawnerSample
{
    int angle;
    int step;

    public LootAngle(int increment)
    {
        step = increment;
        angle = 0;
    }

    public int NextAngle()
    {
        angle = Helpers.WrapAngle(angle + step);
        int currentAngle = angle;
        
        return currentAngle;
    }
}